package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoEnteroDto {
    Integer id;
    LocalDate fecha;
    LocalTime hora;
    Boolean atendido;
    PacienteEnteroDto pacienteEnteroDto;
    MedicoEnteroDto medicoEnteroDto;
}
