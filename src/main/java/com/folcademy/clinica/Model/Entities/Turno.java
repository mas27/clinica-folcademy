package com.folcademy.clinica.Model.Entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name = "turno")
@Data
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    Integer id;


    @Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;
    @Column(name = "hora", columnDefinition = "TIME")
    public LocalTime hora;
    @Column(name = "atendido", columnDefinition = "TINYINT")
    public Boolean atendido;
    @Column(name = "idpaciente", columnDefinition = "INT")
    public Integer idpaciente;
    @Column(name = "idmedico", columnDefinition = "INT")
    public Integer idmedico;

    @OneToMany(fetch = FetchType.LAZY)
    List<Turno> turnos;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente", insertable = false, updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico", insertable = false, updatable = false)
    private Medico medico;

}
