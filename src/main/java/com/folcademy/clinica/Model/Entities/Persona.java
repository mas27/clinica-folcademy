package com.folcademy.clinica.Model.Entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "persona")
@Data
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpersona;
    @Column(name = "dni", columnDefinition = "VARCHAR")
    public String dni = "";
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre = "";
    @Column(name = "apellido", columnDefinition = "VARCHAR")
    public String apellido = "";
}
