package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {

    public PacienteDto entityToDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getTelefono(),
                                ent.getDireccion(),
                                ent.getIdpersona()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setIdpersona(dto.getIdpersona());
        return entity;
    }

    public PacienteEnteroDto entityToEnteroDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteEnteroDto(
                                ent.getId(),
                                ent.getTelefono(),
                                ent.getDireccion(),
                                new PersonaDto(
                                        ent.getPersona().getIdpersona(),
                                        ent.getPersona().getDni(),
                                        ent.getPersona().getNombre(),
                                        ent.getPersona().getApellido()
                                )
                        )
                )
                .orElse(new PacienteEnteroDto());
    }

    public Paciente enteroDtoToEntity(PacienteEnteroDto dto, Integer idPersona){
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setTelefono(dto.getTelefono());
        entity.setDireccion(dto.getDireccion());
        entity.setIdpersona(idPersona);
        return entity;
    }
}

