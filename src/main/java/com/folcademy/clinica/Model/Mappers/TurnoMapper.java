package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                new PacienteEnteroDto(
                                        ent.getPaciente().getId(),
                                        ent.getPaciente().getTelefono(),
                                        ent.getPaciente().getDireccion(),
                                        new PersonaDto(
                                                ent.getPaciente().getPersona().getIdpersona(),
                                                ent.getPaciente().getPersona().getDni(),
                                                ent.getPaciente().getPersona().getNombre(),
                                                ent.getPaciente().getPersona().getApellido()
                                        )
                                ),
                                new MedicoEnteroDto(
                                        ent.getMedico().getId(),
                                        ent.getMedico().getProfesion(),
                                        ent.getMedico().getConsulta(),
                                        new PersonaDto(
                                                ent.getMedico().getPersona().getIdpersona(),
                                                ent.getMedico().getPersona().getDni(),
                                                ent.getMedico().getPersona().getNombre(),
                                                ent.getMedico().getPersona().getApellido()
                                        )
                                )
                        )
                )
                .orElse(new TurnoEnteroDto());
    }

}
