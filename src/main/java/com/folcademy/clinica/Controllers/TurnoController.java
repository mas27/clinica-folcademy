package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnoController {
    private final TurnoService turnoService;


    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @GetMapping("/page")
    public ResponseEntity<Page<TurnoEnteroDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    ) {
        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    @GetMapping("/{idTurno}")
    public ResponseEntity<Page<TurnoEnteroDto>> listarUno(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.listarUno(id));
    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto dto) {
        return ResponseEntity.ok(turnoService.agregar(dto));
    }

    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idTurno") int id,
                                                  @RequestBody TurnoDto dto) {
        return ResponseEntity.ok(turnoService.editar(id, dto));
    }

    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }
}
