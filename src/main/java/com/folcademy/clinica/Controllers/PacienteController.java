package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @GetMapping(value = "/page")
    public ResponseEntity<Page<PacienteEnteroDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    ) {
        return ResponseEntity.ok().body(pacienteService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    @GetMapping("/{idPaciente}")
    public ResponseEntity<Page<PacienteEnteroDto>> listarUno(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    @PostMapping("")
    public ResponseEntity<PacienteEnteroDto> agregar(@RequestBody @Validated PacienteEnteroDto dto) {
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }

    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente") int id,
                                              @RequestBody PacienteEnteroDto dto) {
        return ResponseEntity.ok(pacienteService.editar(id, dto));
    }

    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
