package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @GetMapping("/{idMedico}")
    public ResponseEntity<Page<MedicoEnteroDto>> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    @GetMapping("/page")
    public ResponseEntity<Page<MedicoEnteroDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField)
    {
        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody @Validated MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.editar(id, dto));
    }
    
    @PutMapping("/{idMedico}/consulta/{consulta}")
    public ResponseEntity<Boolean> editarConsulta(@PathVariable(name = "idMedico") int id,
                                                  @PathVariable(name = "consulta") int consulta) {
        return ResponseEntity.ok(medicoService.editarConsulta(id, consulta));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }
}
