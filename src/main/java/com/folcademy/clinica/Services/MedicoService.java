package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.exceptions.NotFoundException;
import com.folcademy.clinica.exceptions.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public Page<MedicoEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField){
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToEnteroDto);
    }

    public Page<MedicoEnteroDto> listarUno(Integer idMedico) {
        if(!medicoRepository.existsById((idMedico)))
            throw new NotFoundException("No existe el medico");

        Pageable pageable = PageRequest.of(0, 1);
        return medicoRepository.findById(idMedico, pageable).map(medicoMapper::entityToEnteroDto);
    }

    public MedicoEnteroDto agregar(MedicoEnteroDto dto) {
        dto.setId(null);

        if (dto.getConsulta() < 0)
            throw new NotFoundException("La consulta no peude ser menor a 0");
        if (Objects.equals(dto.getProfesion(), ""))
            throw new NotFoundException("La profesion no puede estar vacia");

        if (dto.getPersonaDto() != null)
            if (personaRepository.existsById(dto.getPersonaDto().getId()))
                return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto, dto.getPersonaDto().getId())));

        if (dto.getPersonaDto() != null) {
            Persona persona = personaRepository.save(personaMapper.dtoToEntity(dto.getPersonaDto()));
            return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto, persona.getIdpersona())));
        }

        throw new NotFoundException("persona vacia");
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw  new RuntimeException("No existe el medico");

        if (dto.getConsulta()<0)
            throw new ValidationException("La consulta no puede ser menor a 0");

        if(personaRepository.existsById(dto.getPersonaDto().getId())){
            personaRepository.save(personaMapper.dtoToEntity(dto.getPersonaDto()));
            return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto, dto.getPersonaDto().getId())));
        }
        throw new NotFoundException("idPersona incorrecto");

    }

    public boolean editarConsulta(Integer idMedico, Integer consulta) {
        if(!medicoRepository.existsById((idMedico)))
            throw new NotFoundException("No existe el medico");
        if(consulta<0)
            throw new ValidationException("La consulta no puede ser menor a 0");

        Medico entity = medicoRepository.findById(idMedico).get();
        entity.setConsulta(consulta);
        medicoRepository.save(entity);
        return true;

    }

    public boolean eliminar(Integer idMedico) {
        if(!medicoRepository.existsById((idMedico)))
            throw new NotFoundException("No existe el medico");
        medicoRepository.deleteById(idMedico);
        return true;
    }
}
