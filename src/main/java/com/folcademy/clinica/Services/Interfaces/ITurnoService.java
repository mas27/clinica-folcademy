package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;

import java.util.List;

public interface ITurnoService {
    List<TurnoEnteroDto> listarTodos();
}
