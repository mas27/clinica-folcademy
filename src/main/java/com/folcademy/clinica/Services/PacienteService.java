package com.folcademy.clinica.Services;


import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.exceptions.NotFoundException;
import com.folcademy.clinica.exceptions.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaMapper personaMapper, PersonaRepository personaRepository) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
    }

    public Page<PacienteEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField){
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToEnteroDto);
    }

    public Page<PacienteEnteroDto> listarUno(Integer idPaciente) {
        if(!pacienteRepository.existsById((idPaciente)))
            throw new NotFoundException("No existe el paciente");
        Pageable pageable = PageRequest.of(0, 1);
        return pacienteRepository.findById(idPaciente, pageable).map(pacienteMapper::entityToEnteroDto);
    }

    public PacienteEnteroDto agregar(PacienteEnteroDto dto){
        dto.setId(null);
        if (dto.getPersonaDto() != null)
            if (dto.getPersonaDto().getId() != null)
                if (personaRepository.existsById(dto.getPersonaDto().getId()))
                    return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto, dto.getPersonaDto().getId())));

        if (dto.getPersonaDto() != null) {
            Persona persona = personaRepository.save(personaMapper.dtoToEntity(dto.getPersonaDto()));
            return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto, persona.getIdpersona())));
        }
        throw new NotFoundException("persona vacia");
    }

    public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto){
        if(!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("No existe el paciente");

        if(personaRepository.existsById(dto.getPersonaDto().getId())) {
            personaRepository.save(personaMapper.dtoToEntity(dto.getPersonaDto()));
            return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto, dto.getPersonaDto().getId())));
        }

        throw new NotFoundException("idPersona incorrecto");
    }

    public boolean eliminar(Integer idPaciente) {
        if(!pacienteRepository.existsById((idPaciente)))
            throw new NotFoundException("No existe el paciente");
        pacienteRepository.deleteById(idPaciente);
        return true;
    }
}
