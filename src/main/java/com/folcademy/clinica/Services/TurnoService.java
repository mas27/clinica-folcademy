package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import com.folcademy.clinica.exceptions.NotFoundException;
import com.folcademy.clinica.exceptions.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService{
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    public Page<TurnoEnteroDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField){
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(String.valueOf(orderField)));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToEnteroDto);
    }

    public Page<TurnoEnteroDto> listarUno(Integer idTurno) {
        if(!turnoRepository.existsById((idTurno)))
            throw new NotFoundException("No existe el turno");
        Pageable pageable = PageRequest.of(0, 1);
        return turnoRepository.findById(idTurno, pageable).map(turnoMapper::entityToEnteroDto);
    }

    public TurnoDto agregar(TurnoDto dto) {
        if(dto.getFecha().isBefore(LocalDate.now()))
            throw new ValidationException("La fecha no puede ser anterior a la fecha actual");
        dto.setId(null);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }

    public TurnoDto editar(Integer idTurno, TurnoDto dto) {
        if (!turnoRepository.existsById(idTurno))
            throw new NotFoundException("No existe el turno");
        dto.setId(idTurno);
        return turnoMapper.entityToDto(
                turnoRepository.save(
                        turnoMapper.dtoToEntity(dto)
                )
        );
    }

    public boolean eliminar(Integer idTurno) {
        if(!turnoRepository.existsById((idTurno)))
            throw new NotFoundException("No existe el turno");
        turnoRepository.deleteById(idTurno);
        return true;
    }
}
